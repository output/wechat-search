#Wechat Search
##本项目的功能是提供微信公众号基本信息及其最近发布的消息的搜索
##Thanks for some codes from other projects

##使用方法：
* 当然是先 npm install 安装依赖的包
* node server.js 默认情况下，程序监听本地的40000端口
* 使用浏览器或者其他http客户端访问：http://127.0.0.1:40000/search?word=XXXX&page=yyyy 其中XXXX是搜索的关键字，yyyy是页数
* 访问 http://127.0.0.1:40000/detail?openid=zzzz 获取公众号详情（其实这些详情在search时已经都可以取到，但是为了以后单点更新，还是有必要存在的），openid是在搜索时获得的
* 访问 http://127.0.0.1:40000/articles?openid=zzzz&page=yyyy ，不多解释了

##一些说明
* 本程序用到了其他同行的几个函数，由于是电脑中保存的片段代码，已经无法想起原作者是谁，在这里先感谢原作者，如果本程序有代码侵犯了您的权益，请与我联系，我会立即才去相关措施
* 本程序支持运行在本地，以及Heroku、Openshift的免费云主机上
* 所有数据来源于 http://weixin.sogou.com，本程序只是做了一些封装，让几个功能点可以输出JSON格式的数据
* packages.json里面有些依赖包看上去有点奇怪，是的，不加上那些似乎在OpenShift上无法正常运行